<?php 
session_start();

if(!isset($_SESSION['user'])){
		
	if(isset($_SESSION['login_attempt'])){
		if($_SESSION['login_attempt']){
			$login_msg = "Please input login";
			$login_msg_color = "info";
		}else{
			$login_msg = "Error/Invalid Login";
			$login_msg_color = "danger";
			$_SESSION['login_attempt']=true;
		}
	}else{
		$login_msg = "Please input login";
		$login_msg_color = "info";
	}

}else{
	header("location: index.php");
}

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>User Login</title>

		<!-- Bootstrap -->
		<link href="./../Resource/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
		<link href="./../Resource/font_awesome/css/font_awesome.min.css" rel="stylesheet" type="text/css"/>
		
		<style type="text/css">
			#xxx > .input-group{margin-top:5px;}
		</style>
		
	</head>
	
	<body>
		
		<div class="container">
			<div class="col-md-4 col-md-offset-4">
			<br/>
			<br/>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">
							<i class="fa fa-sign-in fa-lg"></i>&nbsp; User Login
						</h3>
					</div>
					<div class="panel-body">
					<?php 
					$warning_color=isset($_SESSION['msg'])?"danger":"info";
					//var_dump($_SESSION);
					?>
						<div class="alert alert-<?php echo $login_msg_color?>">
							<span class="glyphicon glyphicon-warning-sign"></span> <?php echo $login_msg?>
						</div>
						<form action="log-in.php" method="POST" id="xxx">
							<div class="input-group">
								<label for="username" class="input-group-addon" id="username-addon"><span class="glyphicon glyphicon-user"></span></label>
								<input type="text" name="username" class="form-control" placeholder="username" aria-describedby="username-addon" id="username" autofocus/>
							</div>
							<div class="input-group">
								<label for="password" class="input-group-addon" id="password-addon"><i class="fa fa-key"></i></label>
								<input type="password" name="password" class="form-control" placeholder="password" aria-describedby="password-addon" id="password"/>
							</div>
							<div class="input-group">
								<button type="submit" class="btn btn-block btn-default">
									<i class="fa fa-sign-in"></i> Login
								</button>
							</div>
						</form>	
						<p class="text-center">
							<a href="register.php">Create an user account</a>
						</p>
					</div>
					<div class="panel-footer text-center text-<?php echo $login_msg_color?>">
						<i class="fa fa-code fa-lg"></i>The Code Squad
					</div>
				</div>
			
			</div>
		</div>
		
		
		
		
		
		
		
		
		
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="./../Resource/jquery/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="./../Resource/bootstrap/js/bootstrap.min.js"></script>
		
		
		<script type="text/JavaScript">
			
		</script>
		
	</body>
	
</html>