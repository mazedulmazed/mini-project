<?php 

function __autoload($className){
	$file = './../../'.str_replace('\\','/',$className).'.php';
	//echo $file;
	require_once($file);
}

use App\Engine\Core\BluePrint;
use App\Engine\Core\Validation;
use App\Engine\Tool\Debug;
use App\Engine\Tool\Q;

//Debug::d(123);

?>
<?php 
session_start();

if(isset($_SESSION['user'])){
	die("<h1 style='color:red'>You already own an account!</h1>");
}

if($_SERVER['REQUEST_METHOD']==='POST'){
	
	if(count($_POST)===6){
		if( 
			array_key_exists('fullname',$_POST) 
			&&
			array_key_exists('birthdate',$_POST) 
			&&
			array_key_exists('email',$_POST) 
			&&
			array_key_exists('username',$_POST) 
			&&
			array_key_exists('password',$_POST) 
			&&
			array_key_exists('agree',$_POST) 			
		){
			$fullname=$_SESSION['fullname_value']=$_POST['fullname'];
			$birthdate=$_SESSION['birthdate_value']=$_POST['birthdate'];
			$email=$_SESSION['email_value']=$_POST['email'];
			$username=$_SESSION['username_value']=$_POST['username'];
			$password=$_POST['password'];
			
			$result=true;
			if(!empty($fullname) && is_string($fullname) && strlen($fullname)>8){
				$_SESSION['fullname_class']='success';
				$_SESSION['fullname_msg']='Name is OK';
			}else{
				$result=false;
				$_SESSION['fullname_class']='error';
				$_SESSION['fullname_msg']='Please enter your legal name';
			}
			
			if(!empty($birthdate) && is_string($birthdate) && strlen($birthdate)===10 ){
				$_SESSION['birthdate_class']='success';
				$_SESSION['birthdate_msg']='Birthdate is OK';
			}else{
				$result=false;
				$_SESSION['birthdate_class']='error';
				$_SESSION['birthdate_msg']='Please enter valid date';
			}
			
			if(!empty($email) && is_string($email) ){
				$result1 = Validation::is_email_exists($_POST['email']);
				if($result1){
					$result=false;
					$_SESSION['email_class']='error';
					$_SESSION['email_msg']='Email already exists!';
				}else{
					$_SESSION['email_class']='success';
					$_SESSION['email_msg']='Email is OK';
				}
			}else{
				$result=false;
				$_SESSION['email_class']='error';
				$_SESSION['email_msg']='Please enter valid email';
			}
			
			if(!empty($username) && is_string($username) && strlen($username)>4 && strlen($username)<16 ){
				$result1 = Validation::is_user_exists($_POST['username']);
				if($result1){
					$result=false;
					$_SESSION['username_class']='error';
					$_SESSION['username_msg']='Username already exists!';
				}else{
					$_SESSION['username_class']='success';
					$_SESSION['username_msg']='Username is OK';
				}
			}else{
				$result=false;
				$_SESSION['username_class']='error';
				$_SESSION['username_msg']='username should be 4 to 16 characters';
			}
			
			if(!empty($password) && is_string($password) && strlen($password)>4 && strlen($password)<16 ){
				$_SESSION['password_class']='success';
				$_SESSION['password_msg']='password is OK';
			}else{
				$result=false;
				$_SESSION['password_class']='error';
				$_SESSION['password_msg']='password should be 4 to 16 characters';
			}
			if($result){
				//Debug::pd($_POST);
				$q_arr = [
					'tbl'=>'tbl_user',
					'data'=>[
						'username'=>$_POST['username'],
						'password'=>$_POST['password'],
						'email'=>$_POST['email'],
						'birthdate'=>$_POST['birthdate'],
						'fullname'=>$_POST['email'],
					],
				];
				$reg=Q::insert($q_arr);
				if($reg){
					//$result= Q::insert($q_arr);
					//Debug::dd($result);
					echo "<h1 style='color:green'>Registration Success!!! Please <a href='login.php'>Login Here</a></h1>";
				}else{
					echo "<h1 style='color:red'>Registration Failed!!!</h1>";
				}
			}else{
				header("location:register.php");
			}
		}else{
			print_r($_POST);
			die("ERROR!!!");
		}
	}else{
		//print_r($_POST);
		die("<h1 style='color:red'>You Must Fill Out The Form Completely And Agree Terms & Condition!!!</h1>");
	}
}else{
	header("location: register.php");
}

?>
