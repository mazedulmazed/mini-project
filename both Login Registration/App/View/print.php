<?php 
session_start();
function __autoload($className){
	$file = './../../'.str_replace('\\','/',$className).'.php';
	//echo $file;
	require_once($file);
}

use App\Engine\Core\BluePrint;
use App\Engine\Tool\Debug;
use App\Engine\Tool\Q;

if($_SERVER['REQUEST_METHOD']==='GET' && isset($_SESSION['user']) && !empty($_SESSION['user']) )
{
	//echo "OK"."<br/>";
	//Debug::d($_SESSION);
	$_user = $_SESSION['user'];
	//echo $_SESSION['user']."<br/>";

	$array=[
		'tbl'=>'tbl_user',
		//'fields'=>['email','password'],
		'where'=>[ 'username'=>$_user ],
		//'like'=>['username'=>'mi'],
		//'order'=>['password'=>'DESC' , 'email'=>'DESC'],
	];
	
	$obj = new BluePrint;
	$result = $obj->index($array);
	$user=$result[0];
	//Debug::pd($user);
	
	
require_once('../Library/fpdf/fpdf.php');
	
//*****************
//*****************

class PDF extends FPDF
{
	// Page header
	//**********************************
	function Header()
	{

		// Logo
	   // $this->Image('img.jpg',5,5,30,30);
	   // $this->Image('watermark.jpg',75,50);
		
		$this->SetFont('Arial','B',16);
		
		$this->Cell(0,20,'PHP CV Builder',1,1,"C");
	}

	// Page footer
	//**********************************
	function Footer()
	{
		// Position at 1.5 cm from bottom
		$this->SetY(-15);
		// Arial italic 8
		$this->SetFont('Arial','I',8);
		// Page number
		$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
	}
}

// Instanciation of inherited class
$pdf = new PDF();
$pdf->AliasNbPages();

$pdf->AddPage();
$pdf->SetFont('Times','',12);

//*****************
//*****************

	$pdf->SetFont('Arial','',10);
	$pdf->Cell(0,10,'Printed on '.date("d M Y").' at '.date("h:i:sa"),0,1,"R");
	
	
	$pdf->SetFont('Arial','B',13);	
	$pdf->Cell(0,10,'Personal Information',0,1,"C");
	
	$pdf->Ln(5);
	
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(30,10,'Name',0,0,"C");
	
	$pdf->SetFont('Arial','',10);
	$pdf->Cell(30,10,$user->fullname,0,1,"C");
	
	
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(30,10,'Date of Birth',0,0,"C");
	
	$pdf->SetFont('Arial','',10);
	$str = strtotime($user->birthdate);
	$date=date('d M Y');
	$pdf->Cell(30,10,$date,0,1,"C");
	
	
	$pdf->SetFont('Arial','',10);
	
	
	
	
	$pdf->Output();
	
	
	
}else{
	header("location: login.php");
}


?>
